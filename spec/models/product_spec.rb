require 'rails_helper'

RSpec.describe Product, type: :model do
  it { should belong_to(:service) }
  it { should have_many(:bonus_codes) }
  it { should validate_presence_of(:title) }

  describe "#find_bonus_code" do
    context "code exist" do
      it "returns bonus code object"
    end

    context "code doesn't exist" do
      it "returns nil" do

      end
    end
  end
end

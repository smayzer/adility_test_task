require 'rails_helper'

RSpec.describe BonusCode, type: :model do
  it { should validate_inclusion_of(:status).in_array(described_class.statuses.keys) }
  it { should validate_presence_of(:status) }
  it { should validate_presence_of(:code) }
  it { should validate_uniqueness_of(:code) }
  it { should belong_to(:product) }

  it "has two statuses: for_sale, sold" do
    expect(described_class.statuses.keys).to include('for_sale', 'sold')
  end

  describe "#status=(val)" do
    before(:each) do
      @bonus_code = described_class.new
    end
    context "valid status" do
      it "status is set" do
        @bonus_code.status = "bla bla"
        expect(@bonus_code.status).to be_nil
      end
    end

    context "invalid value" do
      it "status is nil" do
        status = 'sold'
        @bonus_code.status = status
        expect(@bonus_code.status).to eql(status)
      end
    end
  end
end

class RtgService < BaseThirdPathService
  def status
    'for_sale'
  end

  def code_exists?
    true
  end
end
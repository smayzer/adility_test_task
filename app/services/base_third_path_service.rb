class BaseThirdPathService
  def initialize(product, code)
    @product, @code = product, code
  end

  def status
    raise 'Need to implement method `status`'
  end

  def code_exists?
    raise 'Need to implement method `code_exists?`'
  end
end
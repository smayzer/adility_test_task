class TvService < BaseThirdPathService
  def status
    'sold'
  end

  def code_exists?
    false
  end
end
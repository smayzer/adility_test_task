class BonusCode < ActiveRecord::Base
  belongs_to :product

  enum status: { for_sale: 0, sold: 1 }

  validates :status, presence: true, inclusion: { in: statuses.keys }
  validates :code, presence: true, uniqueness: true

  def status=(val)
    super(val) rescue super(nil)
  end
end

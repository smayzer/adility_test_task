class Product < ActiveRecord::Base
  belongs_to :service
  has_many :bonus_codes

  validates_presence_of :title

  # return nil if bonus code doesn't exist for product or object of BonusCode 
  def find_bonus_code(_code)    
    case service.try(:name)
    when 'TV'
      bonus_code_tv_strategy(_code)
    when 'RTG'
      bonus_code_rtg_strategy(_code)
    else
      bonus_codes.find_by(code: _code)
    end        
  end

  private

  def bonus_code_tv_strategy(_code)    
    _bonus_code = bonus_codes.find_by(code: _code)
    tv_service = TvService.new(self, _code)
    return nil unless tv_service.code_exists?
    _bonus_code.status = tv_service.status
    _bonus_code
  end

  def bonus_code_rtg_strategy(_code)
    rtg_service = RtgService.new(self, _code)
    return nil unless rtg_service.code_exists?
    BonusCode.new(product: self, code: _code, status: rtg_service.status)
  end
end

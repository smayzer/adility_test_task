class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  rescue_from ActionController::ParameterMissing, with: :render_params_missing
  rescue_form ActiveRecord::RecordNotFound, with: :render_not_found

  private

  def render_params_missing(e)
    render text: e.message, status: 422
  end

  def render_not_found(e)
    render text: e.message, status: 404
  end
end

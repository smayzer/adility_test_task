class BonusCodesController < ApplicationController
  before_action :get_product

  def validate
    bonus_code = @product.find_bonus_code(code_id)
    if bonus_code.nil?
      render nothing: true, status: 404
    elsif bonus_code.status == 'for_sold'
      render nothing: true, status: 403
    else
      render nothing: true, status: 200
    end
  end

  private

  def get_product
    @product = Product.find(product_id)
  end

  def product_id
    params.require(:product_id)
  end

  def code_id
    params.require(:bonus_code)
  end
end

class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.references :service, index: true

      t.timestamps null: false
    end
    add_foreign_key :products, :services
  end
end

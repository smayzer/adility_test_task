class CreateBonusCodes < ActiveRecord::Migration
  def change
    create_table :bonus_codes do |t|
      t.string :code
      t.references :product, index: true
      t.integer :status

      t.timestamps null: false
    end
    add_foreign_key :bonus_codes, :products
    add_index :bonus_codes, :code, unique: true
  end
end

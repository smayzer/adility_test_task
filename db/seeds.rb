third_path_services = ['TV', 'RTG']
ActiveRecord::Base.transaction do
  third_path_services.each { |name| Service.create!(name: name) }
end